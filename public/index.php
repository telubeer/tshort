<?php
include_once(dirname(__FILE__) . '/../configs/prepend.php');

$db = new \App\Db($config);

$router = new \App\Router($_SERVER, $_GET, $_POST);

$app = new \App\Core($router, $db);
$app->run();