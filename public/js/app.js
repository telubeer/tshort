var App = {
    host: {},
    post: function (url) {
        var self = this;
        // поскольку стоит doctype html5 то ie <9 не поддерживаем
        try {
            this.xhr = new XMLHttpRequest();
        } catch (e) {
            alert('Аяксов не завезли');
            return;
        }
        this.xhr.onreadystatechange = function () {
            if (self.xhr.readyState !== 4) {
                self.completeCallback && self.completeCallback.apply(self.host, [self.xhr]);
                return;
            }
            if (self.xhr.status === 200) {
                var result = self.xhr.responseText;
                self.doneCallback && self.doneCallback.apply(self.host, [result, self.xhr]);
            } else {
                self.failCallback && self.failCallback.apply(self.host, [self.xhr]);
            }
        };

        this.xhr.open('POST', '/', true);
        this.xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        this.xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        this.xhr.send('url=' + encodeURIComponent(url));
        return this;
    },
    done: function (callback) {
        this.doneCallback = callback;
        return this;
    },
    fail: function (callback) {
        this.failCallback = callback;
        return this;
    },
    complete: function (callback) {
        this.completeCallback = callback;
        return this;
    },
};