<?php
namespace App;
use \PDO as PDO;
/**
 * Class Db
 * @package App
 */
class Db {
    public static $instance;
    /**
     * @var PDO
     */
    protected $db;


    static function instance(array $config) {
        if (is_null(static::$instance)) {
            static::$instance = new Db($config);
        }
        return static::$instance;
    }

    public function __construct($config)
    {
        $this->db = new PDO($config['dsn']);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    /**
     * @param $query
     * @return bool
     */
    public function exec($query) {
        return $this->db->exec($query);
    }

    /**
     * @param $query
     * @param array $params
     * @return bool|\PDOStatement
     */
    public function prepareAndExecute($query, array $params) {
        $stmt = $this->db->prepare($query);
        if (!empty($params)) {
            foreach ($params as $row) {
                [$name, $param, $type] = $row;
                $stmt->bindValue($name, $param, $type);
            }
        }
        $stmt->execute();
        return $stmt;
    }

    /**
     * @param $value
     * @return string
     */
    public function quote($value) {
        return $this->db->quote($value);
    }

    /**
     * @return integer
     */
    public function lastId() {
        return (int) $this->db->lastInsertId();
    }

}