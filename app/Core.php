<?php

namespace App;

class Core {
    /**
     * @var Db
     */
    public $db;

    /**
     * @var Router
     */
    public $router;

    /**
     * @var Core
     */
    protected static $instance;

    /**
     * @return Core
     * @throws Exception
     */
    public static function  instance() {
        if (is_null(static::$instance)) {
            throw new Exception('core not found');
        }
        return static::$instance;
    }

    public function __construct(Router $router, Db $db)
    {
        $this->router = $router;
        $this->db = $db;
        static::$instance = $this;
    }

    /**
     * Run application
     */
    public function run() {
        try {
            $body = $this->callAction();
        } catch (NotFound $e) {
            http_response_code(404);
            $body = 'not found';
        } catch (BadRequest $e) {
            http_response_code(400);
            $body = 'bad request';
        }
        echo $body;
    }

    /**
     * Run controllers action
     * @return mixed
     * @throws NotFound
     * @throws BadRequest
     */
    protected function callAction() {
        [$class, $action, $params] = $this->router->dispatch();
        /** @var $controller Controller */
        $controller = new $class($this->db);
        if (!is_callable([$controller, $action])) {
            throw new NotFound();
        }
        return call_user_func_array([$controller, $action], $params ?? []);
    }
}