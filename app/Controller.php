<?php
namespace App;
class Controller {

    protected $model;

    /**
     * Controller constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->model = new Tshort(Core::instance()->db);
    }

    /**
     * @return string
     */
    public function index() {
        return include 'index.phtml';
    }

    /**
     * @param $post
     * @return string
     * @throws BadRequest
     * @throws Exception
     */
    public function save($post) {
        if (empty($post['url'])) {
            throw new BadRequest();
        }
        $code = $this->model->save($post['url']);
        return implode('/', [
            Core::instance()->router->getHost(),
            $code
        ]);
    }

    /**
     * @param $code
     * @return null|string
     * @throws NotFound
     */
    public function redirect($code) {
        if (empty($code)) {
            throw new NotFound();
        }
        $url = $this->model->find($code);
        if (empty($url)) {
            throw new NotFound();
        }
        // better to use PSR-7 but its skipped for simplicity
        header('Location: ' . $url);
        return '';
    }
}