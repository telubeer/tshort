<?php

namespace App;

use \PDO as PDO;

class Tshort
{

    const ALPHABET = '23456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ-_';
    const BASE = 51; // strlen(self::ALPHABET)
    const ADDITIVE = 1000000;

    protected $db;

    public function __construct(Db $db)
    {
        $this->db = $db;
    }


    /**
     * find ulr by hash
     *
     * @param $code
     * @return null|string
     */
    public function find($code)
    {
        return $this
            ->fetch(
                'url',
                'id',
                $this->decode($code),
                PDO::PARAM_INT
            );
    }

    /**
     * Save url to database and return encoded id
     *
     * @param $url
     * @return string
     */
    public function save($url)
    {
        $id = $this
            ->fetch(
                'id',
                'url',
                $url,
                PDO::PARAM_STR
            );
        if (empty($id)) {
            $sql = 'INSERT INTO urls (url) VALUES (:url)';
            $this->db->prepareAndExecute($sql, [[':url', $url, PDO::PARAM_STR]]);
            $id = $this->db->lastId();
        }

        return $this->encode($id);
    }

    /**
     * Fetch row from urls table
     *
     * @param string $selectField field name
     * @param string $whereField condition field name
     * @param mixed $value
     * @param integer $type PDO param type
     * @return mixed
     */
    protected function fetch($selectField, $whereField, $value, $type)
    {
        $sql = "SELECT `{$selectField}` FROM urls where `{$whereField}` = :value";
        return $this->db
            ->prepareAndExecute($sql, [[':value', $value, $type]])
            ->fetchColumn();
    }

    /**
     * encode integer to short hash
     * @param $num
     * @return string
     */
    protected function encode($num)
    {
        $num += static::ADDITIVE;
        $str = '';
        while ($num > 0) {
            $str = self::ALPHABET[($num % self::BASE)] . $str;
            $num = (int)($num / self::BASE);
        }
        return $str;
    }

    /**
     * decode short hash to integer
     * @param $str
     * @return bool|float|int
     */
    protected function decode($str)
    {
        $num = 0;
        $len = strlen($str);
        for ($i = 0; $i < $len; $i++) {
            $num = $num * self::BASE + strpos(self::ALPHABET, $str[$i]);
        }
        return $num - static::ADDITIVE;
    }
}
