<?php

namespace App;

/**
 * Class Router
 * @package App
 */
class Router
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    protected $server;
    protected $get;
    protected $post;

    public function __construct($server, $get, $post)
    {
        $this->server = (array) $server;
        $this->get = (array) $get;
        $this->post = (array) $post;

    }

    public function getHost() {
        return $this->server['HTTP_HOST'] ?? 'localhost';
    }

    /**
     * @return array
     * @throws NotFound
     */
    public function dispatch()
    {
        [$method, $uri, $params] = $this->parseRequest();

        if ($method === static::METHOD_POST && !empty($params)) {

            return [Controller::class, 'save', [$params]];

        } else if ($method === static::METHOD_GET && !empty($uri)) {

            return [Controller::class, 'redirect', [$uri]];

        } else if ($method === static::METHOD_GET && empty($params)) {

            return [Controller::class, 'index'];

        }

        throw new NotFound();
    }

    protected function parseRequest()
    {
        $method = $this->getMethod();
        return [
            $method,
            $this->getURI(),
            $this->getData($method),
        ];
    }

    protected function getURI() {
        return ltrim($this->server['REQUEST_URI'] ?? '', '/');
    }

    protected function getMethod()
    {
        return $this->server['REQUEST_METHOD'] ?? 'GET';
    }

    protected function getData($method) {
        return $method === static::METHOD_GET
            ? $this->get ?? []
            : $this->post ?? [];
    }
}