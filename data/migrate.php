<?php
include_once(dirname(__FILE__) . '/../configs/prepend.php');

$sqlitePrefix = 'sqlite:';
if (substr_count($config['dsn'], $sqlitePrefix)) {
    $file = str_replace($sqlitePrefix, '', $config['dsn']);
    if (!file_exists($file)) {
        fclose(fopen($file, 'w'));
    }
}

$db = new \App\Db($config);
$table = <<<EOF
CREATE TABLE urls
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    url VARCHAR(1024) NOT NULL
);
CREATE UNIQUE INDEX urls_url_uindex ON urls (url);
EOF;

$db->exec($table);
