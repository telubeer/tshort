Requirements: git, php7+, composer

##### Clone repo
```
git clone https://telubeer@bitbucket.org/telubeer/tshort.git
```

##### Change directory
```
$ cd tshort
```

##### Install dependencies:
```
$ composer install
```
or with docker
```
$ docker run --rm -v $(pwd):/app composer/composer install --ignore-platform-reqs
```

##### Initialise database
```
$ php -f data/migrate.php
```

or with docker
```
$ docker run --rm -v $(pwd):/src php php -f /src/data/migrate.php 
```

##### Run php dev webserver
```
$ php -S 0.0.0.0:8080 -t public/
```
or with docker
```
$ docker run --rm -p 8080:8080 -v $(pwd):/src php php -S 0.0.0.0:8080 -t /src/public/ 
```

##### Open url in browser:
```
http://127.0.0.1:8080/
```
